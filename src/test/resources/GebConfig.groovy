import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.RemoteWebDriver

System.setProperty("geb.build.reportsDir", "target/geb-reports")
System.setProperty("geb.build.baseUrl", "https://www.nixsolutions.com")
System.setProperty("autoqa.remoteUrl", "http://10.8.5.126:4445/wd/hub")
//System.setProperty("autoqa.remoteUrl", "http://10.12.73.230:4444/wd/hub")

System.setProperty("webdriver.chrome.driver", "./BrowserDrivers/chromedriver.exe")


hubUrl=  new URL(System.getProperty("autoqa.remoteUrl"))

driver = {
   def driver = new ChromeDriver()
    driver.manage().window().maximize()
    return driver
}

environments{
    "remote-chrome"{
        driver ={
            ChromeOptions options = new ChromeOptions()
            DesiredCapabilities capabilities = DesiredCapabilities.chrome()
            capabilities.setCapability(ChromeOptions.CAPABILITY, options)
            def driver = new RemoteWebDriver(hubUrl, capabilities)
            return driver
        }
    }
}