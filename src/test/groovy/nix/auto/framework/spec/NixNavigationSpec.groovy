package nix.auto.framework.spec

import geb.spock.GebReportingSpec
import nix.auto.framework.page.BlogPage
import nix.auto.framework.page.StartPage


class NixNavigationSpec extends GebReportingSpec{

    def  "Navigate to Blog page"(){
        when:
            to StartPage
        and:
            "User clicks to Blog link"()
        then:
            at BlogPage
    }

}
